import React, { Component } from 'react';

import './App.css';
import ApolloClient from "apollo-boost";
import { ApolloProvider } from "react-apollo";

import { Query } from "react-apollo";
import gql from "graphql-tag";


const client = new ApolloClient({
  uri: "https://countries.trevorblades.com/"
});

class App extends Component{
  constructor(props) {
    super(props);
    if (!this.state) {
        this.state = {
        continent: [],
        continentView: true
      };
    }
    
    this.handleChange = this.handleChange.bind(this);

  }

  handleChange(continent) {
    this.setState({ 
      continent: continent,
      continentView: !this.state.continentView
    });
  }

  myCallback = (continent) => {
    this.handleChange(continent);
  }

  showContinentList = (continent) => {
    this.handleChange(continent);
  }

  render() {
    return (
      <ApolloProvider client={client}>
        {this.state.continentView && (<div className="container">
          <h3>Continents</h3>
          <Query
            query={gql`
              {
                continents {
                  countries {
                    name
                  }
                }
              }
            `}
          >

            {({ loading, error, data }) => {
              if (loading) return <p>Loading...</p>;
              if (error) return <p>Error :(</p>;
                return data.continents.map((continent, index) => (
                    <li className="list-group-item" key={index}>
                      <button type="button" key={index} className="btn btn-light" onClick={() => { this.myCallback(data.continents[index].countries) }}>Continent {index}</button>
                    </li>
                ));
              }}
          </Query>
        </div>)}
        {!this.state.continentView && (<div className="container">
          <button type="button" className="btn btn-light" onClick={() => { this.showContinentList([]) }}>Click to list the continents</button>
          <h3>List of Countries</h3>
          <div >
            {this.state.continent.map((country, i) => <div key={i}>{country.name}</div>)}
          </div>
        </div>)}
      </ApolloProvider>
    );
  }
}

 
export default App;